import {Injectable} from '@angular/core';
import {User} from "./models/user.model";
import {Group} from "./models/group.model";
import {HttpClient} from "@angular/common/http";


const API_URL = "http://localhost:8080/users";
const API_URL_GROUPS = "http://localhost:8080/users-groups";
const API_URL_PERMISSIONS = "http://localhost:8080/permissions";

const httpOptions = {
  headers: {'Content-Type': 'application/json'}, responseType: 'json' as 'json'
};

@Injectable({
  providedIn: 'root'
})
export class DataService {
  users: User[] = [];
  groups: Group[] = [];
  permissions: string[] = [];

  constructor(private http: HttpClient) {
  }


  getAllUsers() {
    return this.http.get<any>(API_URL, httpOptions);
  }

  getUserByName(userName: string | null) {
    return this.http.get<any>(API_URL + '/' + userName, httpOptions);
  }

  addUser(user: User) {
    return this.http.post<User>(API_URL,
      user
      , httpOptions);
  }

  removeUser(userId: number) {
    return this.http.delete<number>(API_URL + '/' + userId, httpOptions);
  }

  getAllGroups() {
    return this.http.get<any>(API_URL_GROUPS, httpOptions);
  }

  addGroup(group: Group) {
    return this.http.post<Group>(API_URL_GROUPS,
      group
      , httpOptions);
  }

  getGroupByName(groupName: string | null) {
    return this.http.get<any>(API_URL_GROUPS + '/' + groupName, httpOptions);
  }

  removeGroup(groupId: number) {
    return this.http.delete<number>(API_URL_GROUPS + '/' + groupId, httpOptions);
  }

  assignUserToGroup(userId: number, groupId: number) {
    return this.http.post<any>(API_URL + '/assign-user-group', {
        userId: userId,
        groupId: groupId
      }
      , httpOptions);
  }

  removeUserFromGroup(userId: number, groupId: number) {
    return this.http.post<any>(API_URL + '/unassign-user-group', {
        userId: userId,
        groupId: groupId
      }
      , httpOptions);
  }

  assignPermissionToGroup(groupName: string, permission: string) {
    return this.http.post<any>(API_URL_PERMISSIONS + '/group-permission', {
        name: groupName,
        permission: permission
      }
      , httpOptions);
  }

  removePermissionFromGroup(groupName: string, permission: string) {
    return this.http.post<any>(API_URL_PERMISSIONS + '/remove-permission-group', {
        name: groupName,
        permission: permission
      }
      , httpOptions);
  }

  getAllPermissions() {
    return this.http.get<any>(API_URL_PERMISSIONS, httpOptions);
  }

  assignPermissionToUser(userName: string, permission: string) {
    return this.http.post<any>(API_URL_PERMISSIONS, {
      name: userName,
      permission: permission
    }, httpOptions);
  }

  removePermissionFromUser(userName: string, permission: string) {
    return this.http.post<any>(API_URL_PERMISSIONS + '/remove-permission', {
      name: userName,
      permission: permission
    }, httpOptions);
  }

  createPermission(permission: string) {
    return this.http.post<string>(API_URL_PERMISSIONS + '/' + permission,
      permission
      , httpOptions);
  }

// ... Other CRUD operations for users and groups
}
