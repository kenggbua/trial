import {Component, OnInit} from '@angular/core';
import {DataService} from "../data.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-group-details',
  templateUrl: './group-details.component.html',
  styleUrls: ['./group-details.component.scss']
})
export class GroupDetailsComponent implements OnInit {

  allPermissions: any;

  group: any;

  constructor(public dataService: DataService,
              private route: ActivatedRoute) {

  }

  ngOnInit(): void {
    this.route.queryParamMap.subscribe(params => {
      if (params.get('groupName') != null) {
        this.dataService.getGroupByName(params.get('groupName')).subscribe(user => {
          this.group = user;
        })
      }
    })
    this.dataService.getAllPermissions().subscribe(permissions => {
      this.allPermissions = permissions;
      this.allPermissions = this.allPermissions.filter((per: any) => {
        return !this.group?.permissions.find((e: { permission: any; }) => {
          return per.permission == e.permission
        })
      })
    })
    this.dataService.getAllPermissions().subscribe(permissions => {
      this.allPermissions = permissions;
      this.allPermissions = this.allPermissions.filter((per: any) => {
        return !this.group.permissions.find((e: { permission: any; }) => {
          return per.permission == e.permission
        })
      })
    })
  }

  assignPermissionToGroup(permission: string) {
    this.dataService.assignPermissionToGroup(this.group.groupName, permission).subscribe((c) => {
      this.group.permissions.push(this.allPermissions.filter((per: {
        permission: string;
      }) => per.permission == permission)[0]);
      this.allPermissions.splice(this.allPermissions.indexOf(this.allPermissions.filter((per: {
        permission: string;
      }) => per.permission == permission)[0]), 1)
    });
  }

  removePermissionToGroup(permission: string) {
    this.dataService.removePermissionFromGroup(this.group.groupName, permission).subscribe((c) => {
      this.allPermissions.push(this.group.permissions.filter((per: {
        permission: string;
      }) => per.permission == permission)[0])
      this.group.permissions.splice(this.group.permissions.indexOf(this.group.permissions.filter((per: {
        permission: string;
      }) => per.permission == permission)[0]), 1);
    });
  }

}
