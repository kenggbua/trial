package kevin.Eurofunk;

import kevin.Eurofunk.Modells.Permissions;
import kevin.Eurofunk.Modells.User;
import kevin.Eurofunk.Modells.UserGroups;
import kevin.Eurofunk.Services.UserService;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
//@AutoConfigureWebTestClient
@TestPropertySource("/application-test.yml")
public class UserControllerTest {


    @MockBean
    private UserService userService;

    @Autowired
    MockMvc mockMvc;

    private final Set<UserGroups> userInGroups = new HashSet<>();

    private final Set<Permissions> permissionsSet = new HashSet<>();


    @Test
    public void testCreateUser() throws Exception {
        User newUser = new User(1L, "User", permissionsSet, userInGroups);

        when(userService.createUser("User")).thenReturn(newUser);

        //webClient.post().uri("/users", new UserDetailsDto("User")).exchange().expectStatus().isOk();
        JSONObject jsonObj = new JSONObject();
        mockMvc.perform(post("/users")
                        .content(jsonObj.toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }
}
