package kevin.Eurofunk;

import kevin.Eurofunk.Exceptions.UserAlreadyExistsException;
import kevin.Eurofunk.Modells.Permissions;
import kevin.Eurofunk.Modells.User;
import kevin.Eurofunk.Modells.UserGroups;
import kevin.Eurofunk.Services.UserService;
import kevin.Eurofunk.repositories.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    UserRepository userRepository;

    private final Set<UserGroups> userInGroups = new HashSet<>();

    private final Set<Permissions> permissionsSet = new HashSet<>();


    @Test
    public void testCreateUserSuccess() throws UserAlreadyExistsException {
        User newUser = new User(1L, "User", permissionsSet, userInGroups);

        when(userRepository.findByuserName("User")).thenReturn(Optional.empty());
        when(userRepository.save(any(User.class))).thenReturn(newUser);

        User createdUser = userService.createUser("User");

        assertNotNull(createdUser);
        assertEquals("User", createdUser.getUserName());

        verify(userRepository, times(1)).findByuserName("User");
        verify(userRepository, times(1)).save(any(User.class));

    }

    @Test
    public void testCreateUserException() {
        User newUser = new User(1L, "User", permissionsSet, userInGroups);
        when(userRepository.findByuserName("User")).thenReturn(Optional.of(newUser));
        when(userRepository.save(newUser)).thenReturn(newUser);
        assertThrows(UserAlreadyExistsException.class, () -> {
            userService.createUser("User");
        });

        verify(userRepository, times(1)).findByuserName("User");
        verify(userRepository, never()).save(any(User.class));
    }

}
