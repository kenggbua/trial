package kevin.Eurofunk.repositories;

import kevin.Eurofunk.Modells.Permissions;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PermissionsRepository extends JpaRepository<Permissions, Long> {

    Optional<Permissions> findBypermission(String permissions);

}
